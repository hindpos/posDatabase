const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/posapp');

const { CurrentState } = require('../models/currentState');
const { Product } = require('../models/product');
const { Transaction } = require('../models/transaction');
const { User } = require('../models/user');


CurrentState.findOne().then((doc) => {
  console.log('Current State\n');
  console.log(JSON.stringify(doc, null, 2));
});


Product.findOne().then((doc) => {
  console.log('Product\n');
  console.log(JSON.stringify(doc, null, 2));
});


Transaction.findOne().then((doc) => {
  console.log('Transaction\n');
  console.log(JSON.stringify(doc, null, 2));
});

User.findOne().then((doc) => {
  console.log('User\n');
  console.log(JSON.stringify(doc, null, 2));
});
