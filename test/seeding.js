const mongoose = require('mongoose');

const { CurrentState } = require('../models/currentState');
const { Product } = require('../models/product');
const { Transaction } = require('../models/transaction');
const { User } = require('../models/user');

const { currentStates } = require('../seed/currentState');
const { products } = require('../seed/product');
const { transactions } = require('../seed/transaction');
const { users } = require('../seed/user');

mongoose.connect('mongodb://localhost:27017/posapp', () => {
  mongoose.connection.db.dropDatabase();
  const transaction = new Transaction(transactions[0]);
  transaction.save().then((data) => {
    console.log(data);
  });

  const product = new Product(products[0]);
  product.save().then((data) => {
    console.log(data);
  });

  const currentState = new CurrentState(currentStates[0]);
  currentState.save().then((data) => {
    console.log(data);
  });

  const user = new User(users[1]);
  user.save().then((data) => {
    console.log(data);
  });
});
