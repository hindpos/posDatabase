const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const { OrderSchema } = require('./order');
const { StockSchema } = require('./stock');

const TransactionSchema = mongoose.Schema({
  num: { // an incremental num
    $type: Number,
    required: true,
    unique: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  type: {
    $type: String,
    required: true,
    enum: ['order', 'stock'],
  },
  order: {
    $type: OrderSchema,
  },
  stock: {
    $type: StockSchema,
  },
  buyerId: {
    $type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  sellerId: {
    $type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  user: {
    $type: mongoose.Schema.Types.ObjectId, // buyer in case of stock, and seller in case of order
    ref: 'User',
    required: true,
  },
}, { typeKey: '$type' });

TransactionSchema.pre('save', function transactionValidator(next) {
  if ((this.order === undefined) ^ (this.stock === undefined)) {
    next();
  } else {
    next(new Error('Discount should either be a percentage or a value'));
  }
});

TransactionSchema.plugin(timestamps);
const Transaction = mongoose.model('Transaction', TransactionSchema);

module.exports = { Transaction };
