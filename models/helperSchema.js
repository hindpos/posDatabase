const mongoose = require('mongoose');
const validator = require('validator');
const { checkAll } = require('../utils/utils');

const TaxSchema = mongoose.Schema({
  _id: false,
  type: {
    $type: String,
    enum: ['CGST', 'SGST', 'SAT', 'SET', 'ED', 'VAT', 'CD', 'ET'],
    required: true,
    // * CGST -> Central GST
    // * SGST -> State GST
    // * SAT -> Sales Tax
    // * SET -> Service Tax
    // * ED -> Excise Duty
    // * VAT -> Value Added Tax
    // * CD -> Customs Duty
    // * ET -> Entertainment Tax
  },
  percent: {
    $type: Number,
    min: 0,
    max: 100,
  },
  amount: {
    $type: Number,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
}, { typeKey: '$type' });

TaxSchema.pre('save', function taxValidator(next) {
  if ((this.percent === undefined) ^ (this.amount === undefined)) {
    next();
  } else {
    next(new Error('Tax should either be a percentage or a value'));
  }
});

const ContactSchema = mongoose.Schema({
  _id: false,
  phones: {
    $type: [String],
  },
  emails: {
    $type: [String],
    validate: [val => checkAll(val, validator.isEmail), 'permissions should be alphanumeric'],
  },
}, { typeKey: '$type' });

const AddressSchema = mongoose.Schema({
  _id: false,
  lines: {
    $type: [String],
  },
  landmark: {
    $type: String,
  },
  state: {
    $type: String,
  },
  pincode: {
    $type: Number,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  country: {
    $type: String,
  },
}, { typeKey: '$type' });

const CustomerSchema = mongoose.Schema({
  _id: false,
  name: {
    $type: String,
    trim: true,
    required: true,
  },
  address: {
    $type: AddressSchema,
  },
}, { typeKey: '$type' });

const PaymentSchema = mongoose.Schema({
  _id: false,
  type: {
    $type: String,
    required: true,
    enum: ['cash', 'debit', 'credit', 'cheque', 'dd', 'bank', 'mobile'],
  },
  details: {
    $type: mongoose.Schema.Types.Mixed, // a json converted to String
  },
}, { typeKey: '$type' });

const PriceSchema = mongoose.Schema({
  _id: false,
  global: {
    $type: Boolean,
  },
  customOnGlobal: {
    percent: {
      $type: Number,
      min: -100,
    },
    amount: {
      $type: Number,
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
  },
  custom: {
    $type: Number,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
}, { typeKey: '$type' });

PriceSchema.pre('save', function priceValidator(next) {
  if ((this.global === true) ^ (this.customOnGlobal === undefined) ^ (this.custom === undefined)) {
    next();
  } else {
    next(new Error('Price can be one of the following types : (global, customOnGlobal, custom)'));
  }
});

const ProductStateSchema = mongoose.Schema({
  _id: false,
  product: {
    $type: mongoose.Schema.Types.ObjectId,
    ref: 'Product',
    required: true,
  },
  num: {
    $type: Number,
    min: 0,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  price: {
    $type: Number,
    min: 0,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  tax: {
    $type: Number,
    min: 0,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  priceTotal: {
    $type: Number,
    min: 0,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  total: {
    $type: Number,
    min: 0,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
}, { typeKey: '$type' });

const DiscountSchema = mongoose.Schema({
  _id: false,
  percent: {
    // Percent * 100
    $type: Number,
    min: 0,
    max: 10000,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  amount: {
    $type: Number,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
}, { typeKey: '$type' });

DiscountSchema.pre('save', function discountValidor(next) {
  if ((this.percent === undefined) ^ (this.amount === undefined)) {
    next();
  } else {
    next(new Error('Discount should either be a percentage or a value'));
  }
});

module.exports = {
  TaxSchema,
  AddressSchema,
  ContactSchema,
  CustomerSchema,
  PaymentSchema,
  PriceSchema,
  ProductStateSchema,
  DiscountSchema,
};
