const timestamps = require('mongoose-timestamp');
const mongoose = require('mongoose');
const validator = require('validator');

const { ContactSchema, AddressSchema } = require('./helperSchema');

const UserSchema = mongoose.Schema({
  name: {
    $type: String,
    required: true,
    trim: true,
  },
  userName: {
    $type: String,
    required: true,
    trim: true,
    unique: true,
    validate: [validator.isAlphanumeric, 'userName should be alphanumeric'],
  },
  department: {
    $type: String,
    trim: true,
  },
  contacts: {
    $type: ContactSchema,
  },
  address: {
    $type: AddressSchema,
  },
  permissions: {
    $type: [String],
    required: true,
  },

}, { typeKey: '$type' });

UserSchema.plugin(timestamps);
const User = mongoose.model('User', UserSchema);

module.exports = { User };
