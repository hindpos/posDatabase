const mongoose = require('mongoose');

const { TaxSchema } = require('./helperSchema');

const ProductStockSchema = mongoose.Schema({
  _id: false,
  stockProductId: {
    $type: String,
    trim: true,
  },
  details: {
    $type: mongoose.Schema.Types.Mixed, // a json converted to String
  },
  expiry: {
    $type: Date,
  },
  productId: {
    $type: mongoose.Schema.Types.ObjectId,
    ref: 'Product',
    required: true,
  },
  price: {
    $type: Number,
    required: true,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  tax: {
    $type: [TaxSchema],
    required: true,
  },
  taxTotal: {
    $type: Number,
    required: true,
    min: 0,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  discount: {
    $type: Number,
    required: true,
    min: 0,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  quantity: {
    $type: Number,
    required: true,
    min: 0,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  total: {
    $type: Number,
    required: true,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
}, { typeKey: '$type' });

const StockSchema = mongoose.Schema({
  _id: false,
  stockGivenId: {
    $type: String,
    trim: true,
  },
  stockNum: { // an incremental num
    $type: Number,
    required: true,
    unique: true,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  attachOrderId: {
    $type: mongoose.Schema.Types.ObjectId,
  },
  productList: {
    $type: [ProductStockSchema], // A list of productTransaction object which contains
    required: true,
  },
  price: { // exclusive of all taxes and discount
    $type: Number,
    // We will store the amount of money in paise
    // Comparison on float data$types are bad.
    required: true,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  tax: {
    $type: Number,
    required: true,
    min: 0,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  discount: {
    $type: Number,
    required: true,
    min: 0,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
  totalPrice: { // inclusive of all taxes and discount
    $type: Number,
    required: true,
    validate: {
      validator: Number.isSafeInteger,
    },
  },
}, { typeKey: '$type' });

module.exports = { StockSchema };
