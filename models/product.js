const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const validator = require('validator');

const { TaxSchema, PriceSchema, DiscountSchema } = require('./helperSchema');

const ProductSchema = mongoose.Schema(
  {
    name: {
      $type: String,
      required: true,
      trim: true,
      validate: [validator.isAlphanumeric, 'Name can contain alphanumeric characters and spaces only'],
    },
    stockable: {
    // Set this to true if the product is stockable
      $type: Boolean,
      default: false,
    },
    price: {
      $type: PriceSchema,
      required: true,
    },
    tax: {
      $type: [TaxSchema],
      required: true,
    },
    discount: {
      $type: DiscountSchema,
    },
    reOrderQuantity: {
    // ReOrder quantity if the product is stockable
      $type: Number,
      min: 0,
      required: this.stockable,
    },
    type: {
      $type: String,
      enum: ['Medicine', 'Grocery', 'Service', 'Food', 'Other'],
      default: 'Other',
    },
    details: {
      $type: mongoose.Schema.Types.Mixed, // A json coneverted to String
    },
    user: {
      $type: mongoose.Schema.Types.ObjectId, // buyer in case of stock, and seller in case of order
      ref: 'User',
      required: true,
    // Validation not finalised yet!
    },
  },
  { typeKey: '$type' },
);

ProductSchema.plugin(timestamps);
const Product = mongoose.model('Product', ProductSchema);

module.exports = { Product };
