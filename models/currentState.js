const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const { ProductStateSchema } = require('./helperSchema');

const CurrentStockSchema = mongoose.Schema({
  _id: false,
  stock: {
    $type: mongoose.Schema.Types.ObjectId,
    ref: 'Transaction',
    required: true,
  },
  productList: {
    $type: [ProductStateSchema],
    required: true,
  },
  price: {
    $type: Number,
    min: 0,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  tax: {
    $type: Number,
    min: 0,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
  total: {
    $type: Number,
    min: 0,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
}, { typeKey: '$type' });

const CurrentStateSchema = mongoose.Schema({
  stockList: {
    $type: [CurrentStockSchema],
    required: true,
  },
  total: {
    $type: CurrentStockSchema,
    required: true,
  },
}, { typeKey: '$type' });

CurrentStateSchema.plugin(timestamps);

const CurrentState = mongoose.model('CurrentState', CurrentStateSchema);

module.exports = { CurrentState };
