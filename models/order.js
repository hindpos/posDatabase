const mongoose = require('mongoose');

const { TaxSchema, CustomerSchema, PaymentSchema } = require('./helperSchema');

const ProductOrderSchema = mongoose.Schema({
  _id: false,
  productId: {
    $type: mongoose.Schema.Types.ObjectId,
    ref: 'Product',
    required: true,
  },
  price: {
    $type: Number,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  tax: {
    $type: [TaxSchema],
    required: true,
  },
  taxTotal: {
    $type: Number,
    required: true,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  discount: {
    $type: Number,
    required: true,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  quantity: {
    $type: Number,
    required: true,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  total: {
    $type: Number,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
}, { typeKey: '$type' });

const OrderSchema = mongoose.Schema({
  _id: false,
  orderNum: { // an incremental num
    $type: Number,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  type: {
    $type: String,
    required: true,
    enum: ['new', 'refund', 'discard', 'due', 'refdis', 'empty', 'loss'],
    // Five $types of order possible
    // * new -> New Order
    // * refund -> Refund order | order attachment
    // * discard -> Discard order
    // * due -> Due order | order attachment
    // * refdis -> refund and discard | order attachment
    // * empty -> empty order | needs permission
    // * loss order -> loss order | needs permission
  },
  attachOrderId: {
    $type: mongoose.Schema.Types.ObjectId,
  },
  productList: {
    $type: [ProductOrderSchema], // A list of productTransaction object which contains
    required: true,
  },
  price: { // exclusive of all taxes and discount
    $type: Number,
    // We will store the amount of money in paise. Comparison on float data$types are bad.
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  tax: {
    $type: Number,
    required: true,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  discount: {
    $type: Number,
    required: true,
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  totalPrice: { // inclusive of all taxes and discount
    $type: Number,
    required: true,
    validate: [Number.isSafeInteger, 'Number should be a safe integer.'],
  },
  customer: {
    $type: CustomerSchema,
  },
  payment: {
    $type: PaymentSchema,
    required: true,
  },
}, { typeKey: '$type' });

module.exports = { OrderSchema };
