const mongoose = require('mongoose');

const mongoDbURI = 'mongodb://localhost:27017';
mongoose.Promise = global.Promise;
mongoose.connect(mongoDbURI);

module.exports = { mongoose };
