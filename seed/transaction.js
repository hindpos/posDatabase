const mongoose = require('mongoose');

const { orders } = require('./order');
const { stocks } = require('./stock');

const transactions = [
  {
    num: 1,
    type: 'order',
    order: orders[0],
    buyerId: new mongoose.mongo.ObjectId(),
    sellerId: new mongoose.mongo.ObjectId(),
    user: new mongoose.mongo.ObjectId(),
  },
  {
    num: 2,
    type: 'order',
    stock: stocks[0],
    buyerId: new mongoose.mongo.ObjectId(),
    sellerId: new mongoose.mongo.ObjectId(),
    user: new mongoose.mongo.ObjectId(),
  },
];

module.exports = { transactions };
