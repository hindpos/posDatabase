const mongoose = require('mongoose');
const { taxes, prices, discounts } = require('./helperSchema');

const products = [
  {
    name: 'Metrogyl',
    stockable: true,
    reOrderQuantity: 20,
    price: prices[0],
    tax: taxes,
    discount: discounts[0],
    type: 'Medicine',
    details: {
      colour: 'Brown',
    },
    user: new mongoose.mongo.ObjectId(),
  },
  {
    name: 'Calpol',
    stockable: true,
    reOrderQuantity: 10,
    price: prices[0],
    tax: taxes,
    discount: discounts[0],
    type: 'Medicine',
    details: {
      colour: 'Purple',
    },
    user: new mongoose.mongo.ObjectId(),
  },
];

module.exports = { products };
