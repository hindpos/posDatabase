const mongoose = require('mongoose');

const { taxes } = require('./helperSchema');

const productStocks = [
  {
    stockProductId: 'p01',
    expiry: new Date(),
    productId: new mongoose.mongo.ObjectId(),
    price: 1000,
    tax: taxes,
    taxTotal: 200,
    discount: 10,
    quantity: 50,
    total: 300,
  },
  {
    stockProductId: 'p01',
    expiry: new Date(),
    productId: new mongoose.mongo.ObjectId(),
    price: 1000,
    tax: taxes,
    taxTotal: 200,
    discount: 10,
    quantity: 50,
    total: 300,
  },
];

const stocks = [
  {
    stockGivenId: 'Stock_1',
    stockNum: 1,
    productList: productStocks,
    price: 1000,
    tax: 10,
    discount: 40,
    totalPrice: 5000,
  },
  {
    stockGivenId: 'Stock_1',
    stockNum: 1,
    productList: productStocks,
    price: 1000,
    tax: 10,
    discount: 40,
    totalPrice: 5000,
  },
];

module.exports = { stocks };
