const { contacts, addresses } = require('./helperSchema');

const users = [
  {
    name: 'dokandar Ek',
    userName: 'dokan01',
    department: 'Cough',
    contacts: contacts[0],
    address: addresses[0],
    permissions: 'non-admin',
  },
  {
    name: 'dokandar Dui',
    userName: 'dokan02',
    department: 'Cold',
    contacts: contacts[1],
    address: addresses[1],
    permissions: 'admin',
  },
];

module.exports = { users };
