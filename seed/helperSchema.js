const mongoose = require('mongoose');

const taxes = [
  {
    type: 'CGST',
    amount: 100,
    total: 200,
  },
  {
    type: 'CGST',
    percent: 40,
    total: 200,
  },
];

const contacts = [
  {
    phones: ['+91987654321', '+910985461234'],
    emails: ['dummy01@crypt.email', 'dummy02@crypt.email'],
  },
  {
    phones: ['+91987654300', '+910985461200'],
    emails: [],
  },
];

const addresses = [
  {
    lines: ['11 Ram Lal Dey Street', 'Dum Dum'],
    landmark: 'Gorabazzar',
    state: 'West Bengal',
    pincode: 700028,
    country: 'India',
  },
  {
    lines: ['12 Ram Lal Dey Street', 'Dum Dum'],
    landmark: 'Gorabazzar',
    state: 'West Bengal',
    pincode: 700028,
    country: 'India',
  },
];

const customers = [
  {
    name: 'Sayan Naskar',
    address: addresses[0],
  },
  {
    name: 'Debdut Karmakar',
    address: addresses[1],
  },
];

const payments = [
  {
    type: 'cash',
    details: {
      rupee100: 3,
      rupee500: 2,
    },
  },
  {
    type: 'cash',
    details: {
      rupee100: 3,
      rupee500: 2,
    },
  },
];

const prices = [
  {
    global: false,
    customOnGlobal: {
      percent: 5,
      amount: 50,
    },
  },
  {
    global: false,
    customOnGlobal: {
      percent: 5,
      amount: 50,
    },
  },
];

const productStates = [
  {
    product: new mongoose.mongo.ObjectId(),
    num: 3,
    price: 50,
    tax: 10,
    priceTotal: 180,
    total: 200,
  },
  {
    product: new mongoose.mongo.ObjectId(),
    num: 4,
    price: 60,
    tax: 20,
    priceTotal: 320,
    total: 350,
  },
];

const discounts = [
  {
    percent: 10,
  },
  {
    amount: 50,
  },
];

module.exports = {
  taxes, addresses, contacts, customers, payments, prices, productStates, discounts,
};
