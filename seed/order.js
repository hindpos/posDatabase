const mongoose = require('mongoose');

const { taxes, customers, payments } = require('./helperSchema');

const productOrders = [
  {
    productId: new mongoose.mongo.ObjectId(),
    price: 50,
    tax: taxes,
    taxTotal: 100,
    discount: 30,
    quantity: 5,
    total: 500,
  },
  {
    productId: new mongoose.mongo.ObjectId(),
    price: 50,
    tax: taxes,
    taxTotal: 100,
    discount: 30,
    quantity: 5,
    total: 500,
  },
];

const orders = [
  {
    orderNum: 4,
    type: 'new',
    productList: productOrders,
    price: 300,
    tax: 50,
    discount: 30,
    totalPrice: 1000,
    customer: customers[0],
    payment: payments[0],
  },
  {
    orderNum: 5,
    type: 'new',
    productList: productOrders,
    price: 300,
    tax: 50,
    discount: 30,
    totalPrice: 1000,
    customer: customers[1],
    payment: payments[1],
  },
];

module.exports = { orders };
