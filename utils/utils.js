const checkAll = (val, fn) => {
  if (val !== undefined && val.length > 0) {
    return val.map(fn).reduce((curr, nxt) => curr && nxt);
  }
  return true;
};

module.exports = { checkAll };
