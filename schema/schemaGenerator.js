const {
  typeDefs,
  resolvers,
  modelToType,
  modelToInputType,
} = require('mongoose-to-graphql');

const { CurrentState } = require('../models/currentState');
const { Product } = require('../models/product');
const { Transaction } = require('../models/transaction');
const { User } = require('../models/user');

const CurrentStateGQLSchema = modelToType(CurrentState);
const ProductGQLSchema = modelToType(Product);
const TransactionGQLSchema = modelToType(Transaction);
const UserGQLSchema = modelToType(User);

const CurrentStateGQLInputSchema = modelToInputType(CurrentState);
const ProductGQLInputSchema = modelToInputType(Product);
const TransactionGQLInputSchema = modelToInputType(Transaction);
const UserGQLInputSchema = modelToInputType(User);

module.exports = {
  typeDefs,
  resolvers,
  CurrentStateGQLSchema,
  ProductGQLSchema,
  TransactionGQLSchema,
  UserGQLSchema,
  CurrentStateGQLInputSchema,
  ProductGQLInputSchema,
  TransactionGQLInputSchema,
  UserGQLInputSchema,
};
