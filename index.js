const { CurrentState } = require('./models/currentState');
const { Product } = require('./models/product');
const { Transaction } = require('./models/transaction');
const { User } = require('./models/user');
const GQLSchemas = require('./schema/schemaGenerator');

const models = {
  CurrentState,
  Product,
  Transaction,
  User,
};

module.exports = {
  models,
  GQLSchemas,
};
